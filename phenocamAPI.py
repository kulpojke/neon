"""
This module is a python implementation of the R package phenocamapi.
The usage is the same, and as far as I can tell it bahves the same,
though I have not thouroughly tested it
"""

import requests
import json
import pandas as pd


def get_awb_table(site):
    '''returns Auto-White-Balance (AWB) status of images for PhenoCam sites'''
    phenocam_server = 'http://phenocam.sr.unh.edu/'
    url = '{}/data/archive/{}/ROI/{}-awb.txt'.format(phenocam_server, site, site)
    try:
        stuff = pd.read_csv(url)
    except:
        raise Exception('Dang! no AWB for {}'.format(site))
    return(stuff)


def get_midday_list(site, direct=True):
    '''returns th elist of midday images from phenocam sites'''
    phenocam_server = 'http://phenocam.sr.unh.edu/'
    if direct:
        url = '{}webcam/network/middayimglist/{}'.format(phenocam_server, site)
        try:
            response = requests.get(url)
            stuff = response.json()
        except:
            raise Exception('Oh snap! that failed!  Try direct=False')
    else:
        url = '{}/data/archive/{}/ROI/{}-midday.txt'.format(phenocam_server, site, site)
        try:
            response = requests.get(url)
            stuff = response.text
        except:
            raise Exception('Dang! nothing there for {}'.format(site))
    return(stuff)


def get_phenos():
    '''returns meta-data from phenocam network'''
    url = 'https://phenocam.sr.unh.edu/api/cameras/?limit=10000'
    response = requests.get(url)
    phe = pd.DataFrame(response.json()['results'])
    phe['date_first'] = pd.to_datetime(phe['date_first'])
    phe['date_last'] = pd.to_datetime(phe['date_last'])
    phe = pd.concat([phe.drop(['sitemetadata'], axis=1), phe['sitemetadata'].apply(pd.Series)], axis=1)
    return(phe)


def get_pheno_ts(site, vegType, roiID, kind='3day'):
    ''' site - site name as character string
    vegType - 2-letter character string indicating the vegetation type
    roiID - four-digit integer number indicating the ROI number
    type - a character string indicating what data to be obtained, can be '1day', '3day', or 'roistats'
    returns - a data.table containing phenological data over time '''
    phenocam_server = 'http://phenocam.sr.unh.edu/'
    roiID = str(roiID).zfill(4)
    url = '{}/data/archive/{}/ROI/{}_{}_{}_{}.csv'.format(phenocam_server, site, site, vegType, roiID, kind)
    ts = pd.read_csv(url)
    return(ts)


def get_rois():
    url = 'https://phenocam.sr.unh.edu/api/roilists/?limit=10000'
    response = requests.get(url)
    rois = pd.DataFrame(response.json()['results'])
    return(rois)