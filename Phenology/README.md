# Project Title

Code for working with data from the National Ecological Observation Network (NEON) ( https://data.neonscience.org/home ). Jupyter notebooks explain the code. Python (.py) versions of the code will be referenced in the relative notebooks. 

### Prerequisites
Code is written in Python3, may work on Python2, but probably not.
Some code requires associated .csv files.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Code draws from product user guides provided by NEON.


